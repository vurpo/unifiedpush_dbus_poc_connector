# PoC app with connector for UnifiedPush

This is a PoC for how you can implement an app that uses UnifiedPush to receive push notifications on D-Bus platforms. Note: this app simply sends a desktop notification immediately for every push but real-life applications are not limited to just this, they can have as complicated logic as they want.

For example, upon receiving a push message, a Matrix client could wake up and perform a /sync, decrypt E2EE messages if necessary, update or remove existing notifications, and send new ones.

Read the comments in the code, they contain useful hints to app developers looking for an example of how to use UP!

### D-Bus activation

To have your app be "activatable", so that the distributor can send it push messages while it's not running and it'll automatically start up, your app should install a D-Bus service file inside one of the D-Bus session service directories. These usually include `/usr/share/dbus-1/services/` and `$XDG_RUNTIME_DIR/dbus-1/services/`, `$XDG_DATA_DIR/dbus-1/services/`, and so on, or if you're packaging in a Flatpak for example, `/app/share/dbus-1/services/`. (See the D-Bus docs for the full info.) The file should be named after your app ID, for example `tld.yourdomain.YourApp.service`, and look like the following:

```
[D-BUS Service]
Name=tld.domain.YourApp
Exec=/usr/bin/yourapp --push-message
```

(note the command line flag, read the comments in main.rs for more info on this)
