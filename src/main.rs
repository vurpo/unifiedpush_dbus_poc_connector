use std::{collections::HashMap, fmt};
use std::error::Error;
use std::env;
use std::rc::Rc;

use zbus::{dbus_interface, dbus_proxy, fdo};
use zvariant::Value;

const TOKEN: &str = "AAAAAAA";

#[derive(Debug)]
enum ConnectorError{
    NoDistributors,
    RegistrationFailed(String)
}
impl fmt::Display for ConnectorError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}
impl Error for ConnectorError {
    fn source(&self) -> Option<&(dyn Error + 'static)> { None }
}

#[dbus_proxy]
trait Notifications {
    fn notify(
        &self,
        app_name: &str,
        replaces_id: u32,
        app_icon: &str,
        summary: &str,
        body: &str,
        actions: &[&str],
        hints: HashMap<&str, &Value<'_>>,
        expire_timeout: i32,
    ) -> zbus::Result<u32>;
}

#[dbus_proxy(interface = "org.unifiedpush.Distributor1")]
trait Distributor {
    fn register(
        &self,
        appid: &str,
        token: &str
    ) -> zbus::Result<(String, String)>;
}

enum UPConnection {
    NotRegistered,
    Registered { endpoint: String }
}

struct Connector {
    up_connection: UPConnection,
    connection: Rc<zbus::Connection>
}

#[dbus_interface(name = "org.unifiedpush.Connector1")]
impl Connector {
    fn new_endpoint(&mut self, token: &str, new_endpoint: &str) -> Result<(), zbus::fdo::Error> {
        if token == TOKEN {
            self.up_connection = UPConnection::Registered { endpoint: new_endpoint.to_owned() };
            eprintln!("Received and stored new endpoint: {}", new_endpoint);
        } else {
            eprintln!("Wrong token for NewEndpoint call {}", token);
        }
        Ok(())
    }

    fn unregister(&mut self, token: &str) -> Result<(), zbus::fdo::Error> {
        if token == TOKEN {
            self.up_connection = UPConnection::NotRegistered;
            eprintln!("Distributor unregistered us! Trying to reregister");
            register(&self.connection);
        } else {
            eprintln!("Wrong token for Unregister call {}", token);
        }
        Ok(())
    }

    fn message(&mut self, token: &str, data: &str, _id: &str) -> Result<(), zbus::fdo::Error> {
        println!("Hello!");
        if token == TOKEN {
            let proxy = NotificationsProxy::new(&self.connection)?;
            proxy.notify(
                "vurpio-app",
                0,
                "dialog-information",
                "New push!",
                &format!("{}", data),
                &[],
                HashMap::new(),
                5000,
            )?;
        }
        Ok(())
    }
}

fn register(conn: &zbus::Connection) -> Result<String, Box<dyn Error>> {
    let dbus_proxy = fdo::DBusProxy::new(conn)?;
    let names = dbus_proxy.list_names()?;
    let mut distributors = names.iter()
        .filter(|name| name.starts_with("org.unifiedpush.Distributor."));

    // This line simply selects the first distributor available.
    // You should not do this, but have some sort of UI to choose one of the available ones
    // (if there are more than one)
    if let Some(selected_distributor) = distributors.next() {
        eprintln!("Selected distributor {:?}", selected_distributor);
        let distributorname: String = selected_distributor.strip_prefix("org.unifiedpush.Distributor.").unwrap().to_owned(); // unwrap, this is always valid
        let token = TOKEN; // randomly chosen
        let proxy = DistributorProxy::new_for(
            conn,
            selected_distributor,
            "/org/unifiedpush/Distributor"
        )?;
        if let Ok(result) = proxy.register("io.vurp.NotificationTest", token) {
            if result.0 == "NEW_ENDPOINT" {
                eprintln!("Sent registration, waiting for new endpoint");
                Ok(distributorname)
            } else {
                Err(Box::new(ConnectorError::RegistrationFailed(format!("{}: {}", result.0, result.1))))
            }
        } else {
            Err(Box::new(ConnectorError::RegistrationFailed("D-Bus call failed".to_owned())))
        }
    } else {
        Err(Box::new(ConnectorError::NoDistributors))
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    // To app developers:
    // Implement some kind of command line flag like this,
    // to launch your app in an "invisible" or "background" mode
    // and have your app quit after doing its background stuff when this flag is used.
    // After all, the reason for having push notifications is so
    // your app doesn't have to run in the background all the time!
    let quit_after_message = env::args().find(|arg| arg == "--push-message").is_some();

    let connection = Rc::new(zbus::Connection::new_session()?);

    let mut object_server = zbus::ObjectServer::new(&connection);
    let r = Connector { up_connection: UPConnection::NotRegistered, connection: connection.clone() };
    object_server.at("/org/unifiedpush/Connector", r)?;

    fdo::DBusProxy::new(&connection)?.request_name(
        "io.vurp.NotificationTest",
        fdo::RequestNameFlags::ReplaceExisting.into(),
    )?;

    let _name = register(&connection)?;
    loop {
        let message = connection.receive_message();
        match message {
            Ok(message) => {
                object_server.dispatch_message(&message);
                if quit_after_message {
                    if let Ok(header) = message.header() {
                        if let Ok(Some("org.unifiedpush.Connector1")) = header.interface() {
                            eprintln!("Message received and we're in push message mode, quitting!");
                            break;
                        }
                    }
                }
            },
            Err(error) => eprintln!("{}", error),
        }
    }

    Ok(())
}
